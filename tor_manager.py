#! /usr/bin/env python2
"""
tor_switcher.py reloaded and refactored by Rupe to work with toriptables2.py.
tor_manager.py is a light GUI interface for issuing NEWNYM signals over TOR's control port.
Useful for making any DoS attack look like a DDoS attack.
"""

from commands import getoutput
from json import load
from random import random
from ScrolledText import ScrolledText
from subprocess import call
from telnetlib import Telnet
from thread import start_new_thread
from time import localtime, sleep
from Tkinter import *
from tkMessageBox import showerror
from urllib2 import URLError, urlopen


class TorConfig(object):

  local_dnsport = "53"  # DNSPort
  virtual_net = "10.0.0.0/10"  # VirtualAddrNetwork
  local_loopback = "127.0.0.1"  # Local loopback
  non_tor_net = ["192.168.0.0/16", "172.16.0.0/12"]
  non_tor = ["127.0.0.0/9", "127.128.0.0/10", "127.0.0.0/8"]
  tor_uid = getoutput("id -ur debian-tor")  # Tor user uid
  trans_port = "9040"  # Tor port


class TorIptables(TorConfig):

  def __init__(self):
    super(TorConfig, self).__init__()

  def flush_iptables_rules(self):
    call(["iptables", "-F"])
    call(["iptables", "-t", "nat", "-F"])

  def load_iptables_rules(self):
    self.flush_iptables_rules()
    if self.non_tor_net[0] not in self.non_tor:
      self.non_tor.extend(self.non_tor_net)

    # See https://trac.torproject.org/projects/tor/wiki/doc/TransparentProxy#WARNING
    # See https://lists.torproject.org/pipermail/tor-talk/2014-March/032503.html
    call([
        "iptables", "-I", "OUTPUT", "!", "-o", "lo", "!", "-d",
        self.local_loopback, "!", "-s", self.local_loopback, "-p", "tcp", "-m",
        "tcp", "--tcp-flags", "ACK,FIN", "ACK,FIN", "-j", "DROP"
    ])
    call([
        "iptables", "-I", "OUTPUT", "!", "-o", "lo", "!", "-d",
        self.local_loopback, "!", "-s", self.local_loopback, "-p", "tcp", "-m",
        "tcp", "--tcp-flags", "ACK,RST", "ACK,RST", "-j", "DROP"
    ])

    call([
        "iptables", "-t", "nat", "-A", "OUTPUT", "-m", "owner", "--uid-owner",
        "%s" % self.tor_uid, "-j", "RETURN"
    ])
    call([
        "iptables", "-t", "nat", "-A", "OUTPUT", "-p", "udp", "--dport",
        self.local_dnsport, "-j", "REDIRECT", "--to-ports", self.local_dnsport
    ])

    for net in self.non_tor:
      call([
          "iptables", "-t", "nat", "-A", "OUTPUT", "-d", "%s" % net, "-j",
          "RETURN"
      ])

    call([
        "iptables", "-t", "nat", "-A", "OUTPUT", "-p", "tcp", "--syn", "-j",
        "REDIRECT", "--to-ports", "%s" % self.trans_port
    ])

    call([
        "iptables", "-A", "OUTPUT", "-m", "state", "--state",
        "ESTABLISHED,RELATED", "-j", "ACCEPT"
    ])

    for net in self.non_tor:
      call(["iptables", "-A", "OUTPUT", "-d", "%s" % net, "-j", "ACCEPT"])

    call([
        "iptables", "-A", "OUTPUT", "-m", "owner", "--uid-owner",
        "%s" % self.tor_uid, "-j", "ACCEPT"
    ])
    call(["iptables", "-A", "OUTPUT", "-j", "REJECT"])


class Switcher(Tk):

  def __init__(self):
    Tk.__init__(self)
    self.resizable(0, 0)
    self.title(string=".o0O| TOR IP Switcher |O0o.")

    self.host = StringVar()
    self.port = IntVar()
    self.passwd = StringVar()
    self.time = DoubleVar()

    self.host.set('localhost')
    self.port.set('9051')
    self.passwd.set('')
    self.time.set('30')
    cf = LabelFrame(
        self, text='Control', relief=GROOVE, labelanchor='nw', padx=4, pady=8)
    cf.grid(rowspan=5, pady=8)
    Label(cf, text='Host:').grid(row=1, column=1, sticky=E)
    Label(cf, text='Port:').grid(row=2, column=1, sticky=E)
    Label(cf, text='Password:').grid(row=3, column=1, sticky=E)
    Label(cf, text='Interval:').grid(row=4, column=1, sticky=E)

    Entry(cf, textvariable=self.host).grid(row=1, column=2, columnspan=2)
    Entry(cf, textvariable=self.port).grid(row=2, column=2, columnspan=2)
    Entry(
        cf, textvariable=self.passwd, show='*').grid(
            row=3, column=2, columnspan=2)
    Entry(cf, textvariable=self.time).grid(row=4, column=2, columnspan=2)

    gf = LabelFrame(self, text='Services', relief=GROOVE, labelanchor='nw')
    gf.grid(rowspan=5, padx=4, pady=8)

    Button(gf, text='Start', command=self.start).grid(row=5, column=1)
    Button(gf, text='Stop', command=self.stop).grid(row=5, column=2)
    Button(gf, text='Load', command=self.load).grid(row=5, column=3)
    Button(gf, text='Flush', command=self.flush).grid(row=5, column=4)

    sf = LabelFrame(self, text='Status', relief=GROOVE, labelanchor='nw')
    sf.grid(rowspan=5, padx=4, pady=8)

    self.output = ScrolledText(
        sf,
        foreground="white",
        background="black",
        highlightcolor="white",
        highlightbackground="purple",
        wrap=WORD,
        height=8,
        width=38)
    self.output.grid(row=1, column=4, rowspan=4, padx=4, pady=4)

  def load(self):
    start_new_thread(self.load_tables, ())
    self.write('Anonymizer status [ ON ]')

  def load_tables(self):
    TorIptables().load_iptables_rules()

  def flush(self):
    start_new_thread(self.flush_rules, ())
    self.write('Anonymizer status [ OFF ]')

  def flush_rules(self):
    TorIptables().flush_iptables_rules()

  def start(self):
    self.write('TOR Switcher starting.')
    self.ident = random()
    start_new_thread(self.newnym, ())

  def stop(self):
    try:
      self.write('TOR Switcher stopping.')
    except:
      pass
    self.ident = random()

  def write(self, message):
    t = localtime()
    try:
      self.output.insert(END,
                         '[%02i:%02i:%02i] %s\n' % (t[3], t[4], t[5], message))
      self.output.yview(MOVETO, 1.0)
    except:
      print('[%02i:%02i:%02i] %s\n' % (t[3], t[4], t[5], message))

  def error(self):
    showerror('TOR IP Switcher', 'Tor daemon not running!')

  def newnym(self):
    key = self.ident
    host = self.host.get()
    port = self.port.get()
    passwd = self.passwd.get()
    interval = self.time.get()

    try:
      telnet = Telnet(host, port)
      if passwd == '':
        telnet.write("AUTHENTICATE\r\n")
      else:
        telnet.write("AUTHENTICATE \"%s\"\r\n" % (passwd))
      res = telnet.read_until('250 OK', 5)

      if res.find('250 OK') > -1:
        self.write('AUTHENTICATE accepted.')
      else:
        self.write('Control responded,' + "\n"
                   'Incorrect password: "%s"' % (passwd))
        key = self.ident + 1
        self.write('Quitting.')
    except Exception:
      self.write('There was an error!')
      self.error()
      key = self.ident + 1
      self.write('Quitting.')

    while key == self.ident:
      try:
        telnet.write("signal NEWNYM\r\n")
        res = telnet.read_until('250 OK', 5)
        if res.find('250 OK') > -1:
          try:
            my_new_ident = load(urlopen('https://check.torproject.org/api/ip'))['IP']
          except (URLError, ValueError):
            my_new_ident = getoutput('wget -qO - ident.me')
          self.write('Your IP is %s' % (my_new_ident))
        else:
          key = self.ident + 1
          self.write('Quitting.')
        sleep(interval)
      except Exception, ex:
        self.write('There was an error: %s.' % (ex))
        key = self.ident + 1
        self.write('Quitting.')

    try:
      telnet.write("QUIT\r\n")
      telnet.close()
    except:
      pass


if __name__ == '__main__':
  mw = Switcher()
  mw.mainloop()
  mw.stop()
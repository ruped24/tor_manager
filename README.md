# Tor Manager

![](https://img.shields.io/badge/tor__manager-python_2.7-blue.svg?style=flat-square) ![](https://img.shields.io/badge/dependencies-toriptables2_python--tk_tor-orange.svg?style=flat-square) ![GitHub stars](https://img.shields.io/github/stars/ruped24/tor_ip_switcher.svg?style=social) 

tor_manager is useful for making any DoS attack look like a DDoS attack. Works with [toriptables2](https://bitbucket.org/ruped24/toriptables2g/src).
***

### Dependencies:
`apt install tor python-tk`

`git clone https://bitbucket.org/ruped24/toriptables2g/`

### Setup:
1. [Setup ControlPort](https://github.com/ruped24/tor_ip_switcher#setup-controlport)


2. Start toriptables2


3. Start tor_manager


### [Usage](https://drive.google.com/file/d/1fHtOvukq0j3dcSKk6Yw_d2L3JuXnjNav/view):
Method One: Run-as a background job and disown
```bash
sudo tor_manager.py &
```
```bash
disown
```

Method Two: Run-as a screen session detached
```bash
sudo screen -dmS "tormanager" tor_manager.py
```
## [Screenshot](https://drive.google.com/file/d/0B79r4wTVj-CZdUtGU3p6WldHX2s/view)
## [Troubleshooting](https://github.com/ruped24/tor_ip_switcher/wiki/Troubleshooting)

***